<?php
/**
 * Created by PhpStorm.
 * User: G.Haritonov
 * Date: 14.11.2016
 * Time: 13:32
 */

namespace YmlBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use YmlBundle\Container\SettingsContainer;
use Symfony\Component\Console\Input\ArrayInput;


class DeployCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('yml:deploy')
            ->setDescription('Deploy yml file')
            ->addArgument(
                'siteId',
                InputArgument::REQUIRED,
                'Site Id'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $siteSettings = [];
        $site = $input->getArgument('siteId');

        $output->writeln('Deployment for site: ' . $site);

        $em = $this->getContainer()->get('doctrine')->getManager();
        $settings = $em->getRepository('YmlBundle:Settings')->findOneBySite($site);
        $siteSettings['name'] = $settings->getSite()->getSiteName();
        $siteSettings['url'] = $settings->getSite()->getUrl();
        $settings = $settings->getValue();
        $settings = json_decode($settings, 1);
        $settings = array_merge($siteSettings, $settings);
        $settings = new SettingsContainer($settings);
        $ymldeployer = $this->getContainer()->get('ymldeployer');
        $ymldeployer->deploy($settings);

        $output->writeln('Deployment finished.');
    }
}