<?php
/**
 * Created by PhpStorm.
 * User: G.Haritonov
 * Date: 14.11.2016
 * Time: 13:32
 */

namespace YmlBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use YmlBundle\Container\SettingsContainer;
use Symfony\Component\Console\Input\ArrayInput;


class RunCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('yml:run')
            ->setDescription('Generate yml file')
            ->addArgument(
                'siteId',
                InputArgument::REQUIRED,
                'Site Id'
            )
            ->addArgument(
                'deploy',
                InputArgument::OPTIONAL,
                'Deploy file'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //TODO: settings handling like a object
        $siteSettings = [];
        $output->writeln('Starting');

        $site = $input->getArgument('siteId');

        $output->writeln('Site Id = ' . $site);

        $em = $this->getContainer()->get('doctrine')->getManager();
        $settings = $em->getRepository('YmlBundle:Settings')->findOneBySite($site);
        $siteSettings['name'] = $settings->getSite()->getSiteName();
        $siteSettings['url'] = $settings->getSite()->getUrl();
        $settings = $settings->getValue();
        $settings = json_decode($settings, 1);
        $settings = array_merge($siteSettings, $settings);
        $settings = new SettingsContainer($settings);

        $output->writeln('Crawling...');

        $crawler = $this->getContainer()->get('crawler');
        $crawler->setParseRules($settings);
        $crawler->setURL($settings->base_url);
        $crawler->setupMeta();
        $crawler->go();
        $info = $crawler->getResult();

        $output->writeln('Processing...');

        $infoHandler = $this->getContainer()->get('infohandler');
        $info = $infoHandler->process($info);

        $output->writeln('Generation...');

        $ymlformatter = $this->getContainer()->get('ymlformatter');
        $xml = $ymlformatter->create($info);

        $output->writeln('File generated at: ' . $xml);

        if ($input->getArgument('deploy')) {

            $command = $this->getApplication()->find('yml:deploy');

            $arguments = array(
                'command' => 'yml:deploy',
                'siteId'  => $site,
            );

            $greetInput = new ArrayInput($arguments);
            $command->run($greetInput, $output);

        }
        $output->writeln('Do not panic. You will be assimilated. Resistance is futile. Thank you for your cooperation.');
    }
}