<?php

namespace YmlBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Crawl
 *
 * @ORM\Table(name="crawl", indexes={@ORM\Index(name="site", columns={"site_id"})})
 * @ORM\Entity
 */
class Crawl
{
    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="rules", type="string", length=255, nullable=true)
     */
    private $rules;

    /**
     * @var string
     *
     * @ORM\Column(name="list", type="string", length=255, nullable=true)
     */
    private $list;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \YmlBundle\Entity\Site
     *
     * @ORM\ManyToOne(targetEntity="YmlBundle\Entity\Site")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="site_id", referencedColumnName="id")
     * })
     */
    private $site;



    /**
     * Set url
     *
     * @param string $url
     *
     * @return Crawl
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set rules
     *
     * @param string $rules
     *
     * @return Crawl
     */
    public function setRules($rules)
    {
        $this->rules = $rules;

        return $this;
    }

    /**
     * Get rules
     *
     * @return string
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * Set list
     *
     * @param string $list
     *
     * @return Crawl
     */
    public function setList($list)
    {
        $this->list = $list;

        return $this;
    }

    /**
     * Get list
     *
     * @return string
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set site
     *
     * @param \YmlBundle\Entity\Site $site
     *
     * @return Crawl
     */
    public function setSite(\YmlBundle\Entity\Site $site = null)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return \YmlBundle\Entity\Site
     */
    public function getSite()
    {
        return $this->site;
    }
}
