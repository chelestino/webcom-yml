<?php

namespace YmlBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * YmlFieldToType
 *
 * @ORM\Table(name="yml_field_to_type", indexes={@ORM\Index(name="type", columns={"type_id"}), @ORM\Index(name="field", columns={"field_id"})})
 * @ORM\Entity
 */
class YmlFieldToType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \YmlBundle\Entity\YmlFields
     *
     * @ORM\ManyToOne(targetEntity="YmlBundle\Entity\YmlFields")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field_id", referencedColumnName="id")
     * })
     */
    private $field;

    /**
     * @var \YmlBundle\Entity\YmlType
     *
     * @ORM\ManyToOne(targetEntity="YmlBundle\Entity\YmlType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * })
     */
    private $type;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set field
     *
     * @param \YmlBundle\Entity\YmlFields $field
     *
     * @return YmlFieldToType
     */
    public function setField(\YmlBundle\Entity\YmlFields $field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return \YmlBundle\Entity\YmlFields
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set type
     *
     * @param \YmlBundle\Entity\YmlType $type
     *
     * @return YmlFieldToType
     */
    public function setType(\YmlBundle\Entity\YmlType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \YmlBundle\Entity\YmlType
     */
    public function getType()
    {
        return $this->type;
    }

    public function __toString() {
        return $this->field->getField();
    }
}
