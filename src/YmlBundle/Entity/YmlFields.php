<?php

namespace YmlBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * YmlFields
 *
 * @ORM\Table(name="yml_fields")
 * @ORM\Entity
 */
class YmlFields
{
    /**
     * @var string
     *
     * @ORM\Column(name="field", type="string", length=255, nullable=true)
     */
    private $field;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="optional", type="string", length=255, nullable=true)
     */
    private $optional;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * Set field
     *
     * @param string $field
     *
     * @return YmlFields
     */
    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set optional
     *
     * @param string $optional
     *
     * @return YmlFields
     */
    public function setOptional($optional)
    {
        $this->optional = $optional;

        return $this;
    }

    /**
     * Get optional
     *
     * @return string
     */
    public function getOptional()
    {
        return $this->optional;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return YmlFields
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
