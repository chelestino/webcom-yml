<?php
/**
 * Created by PhpStorm.
 * User: Alexey.Palshin
 * Date: 11.11.2016
 * Time: 16:12
 */

namespace YmlBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class YmlMenuBuilder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->addChild('Home', array('route' => 'site_index'));
//        $menu->addChild('Settings', array('route' => 'settings'));

        return $menu;
    }
}