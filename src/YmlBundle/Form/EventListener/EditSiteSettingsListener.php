<?php

namespace YmlBundle\Form\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventDispatcher;

class EditSiteSettingsListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
//            FormEvents::PRE_SET_DATA => 'onPreSetData',
//            FormEvents::PRE_SUBMIT   => 'onPreSubmit',
            FormEvents::POST_SUBMIT       => 'onPostSubmit',
        );
    }

    public function onPostSubmit(FormEvent $event)
    {


        $allFields  = $event->getForm();

        $a =[];



        foreach ($allFields as $index => $field) {
            $a[$index] = $field->getData();
        }
        \Doctrine\Common\Util\Debug::dump();

    }

}