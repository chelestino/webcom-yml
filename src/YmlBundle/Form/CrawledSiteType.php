<?php
/**
 * Created by PhpStorm.
 * User: Alexey.Palshin
 * Date: 24.10.2016
 * Time: 10:15
 */

namespace YmlBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class CrawledSiteType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('url', TextType::class, array(
                'mapped' => false,
            )
        );


        $builder->get('url')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event){
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $site = $event->getForm()->getData();

                echo '<div class="crawled-site"><iframe src="' . $site . '" width="630" height="630" ></iframe></div>';
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }
}