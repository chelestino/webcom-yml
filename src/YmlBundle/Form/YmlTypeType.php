<?php
/**
 * Created by PhpStorm.
 * User: Alexey.Palshin
 * Date: 10.10.2016
 * Time: 11:00
 */

namespace YmlBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use YmlBundle\Entity\YmlType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use YmlBundle\Form\EventListener\EditSiteSettingsListener;

class YmlTypeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', EntityType::class, [
                    'class' => 'YmlBundle:YmlType',
                ]
            )
            ->add('company', TextType::class, [
                    'mapped' => false,
                ]
            )
            ->add('base_url', TextType::class, [
                    'mapped' => false,
                ]
            )
            ->add('nav_chain', TextType::class, [
                    'mapped' => false,
                ]
            )
            ->add('content_html', TextType::class, [
                    'mapped' => false,
                ]
            )
            ->add('categories', CollectionType::class, [
                        'mapped' => false,
                        'allow_add'    => true,
                        'entry_type'   => EntryType::class,
                        'entry_options'  => [
                            'fields' => [
                                [
                                    'name' => 'category',
                                    'type'    => TextType::class,
                                    'options' => []
                                ], [
                                    'name' => 'category_id',
                                    'type'    => TextType::class,
                                    'options' => []
                                ], [
                                    'name' => 'category_parentId',
                                    'type'    => TextType::class,
                                    'options' => []
                                ]
                            ],
                            'attr' => [
                                'class' => 'collection-fields-wrapper'
                            ],
                        ],
                    ]
                )
            ->add('currencies', CollectionType::class, [
                        'mapped' => false,
                        'allow_add'    => true,
                        'entry_type'   => EntryType::class,
                        'entry_options'  => [
                            'fields' => [
                                [
                                    'name' => 'currency_id',
                                    'type'    => TextType::class,
                                    'options' => []
                                ], [
                                    'name' => 'currency_rate',
                                    'type'    => TextType::class,
                                    'options' => []
                                ]
                            ],
                            'attr' => [
                                'class' => 'collection-fields-wrapper'
                            ],
                        ],
                    ]
                )
            ->add('delivery_options', CollectionType::class, [
                        'mapped' => false,
                        'allow_add'    => true,
                        'entry_type'   => EntryType::class,
                        'entry_options'  => [
                            'fields' => [
                                [
                                    'name' => 'cost',
                                    'type'    => TextType::class,
                                    'options' => [
                                        'label' => 'стоимость доставки в рублях'
                                    ]
                                ], [
                                    'name' => 'days',
                                    'type'    => TextType::class,
                                    'options' => [
                                        'label' => 'срок доставки в рабочих днях'
                                    ]
                                ], [
                                    'name' => 'order-before',
                                    'type'    => TextType::class,
                                    'options' => [
                                        'label' => ''
                                    ]
                                ]
                            ],
                            'attr' => [
                                'class' => 'collection-fields-wrapper'
                            ],
                        ],
                    ]
                )
            ->add('optional_site_fields', CollectionType::class, [
                        'mapped' => false,
                        'allow_add'    => true,
                        'entry_type'   => EntryType::class,
                        'entry_options'  => [
                            'fields' => [
                                [
                                    'name' => 'field',
                                    'type'    => TextType::class,
                                    'options' => []
                                ], [
                                    'name' => 'value',
                                    'type'    => TextType::class,
                                    'options' => []
                                ]
                            ],
                            'attr' => [
                                'class' => 'collection-fields-wrapper'
                            ],
                        ],
                    ]
                )
            ->add('optional_type_fields', CollectionType::class, [
                        'mapped' => false,
                        'allow_add'    => true,
                        'entry_type'   => EntryType::class,
                        'entry_options'  => [
                            'fields' => [
                                [
                                    'name' => 'field',
                                    'type'    => TextType::class,
                                    'options' => []
                                ], [
                                    'name' => 'value',
                                    'type'    => TextType::class,
                                    'options' => []
                                ]
                            ],
                            'attr' => [
                                'class' => 'collection-fields-wrapper'
                            ],
                        ],
                    ]
                )
//            ->addEventSubscriber(new EditSiteSettingsListener())
        ;


        $formModifier = function (FormInterface $form, $ymlType = null) use ($options){
            foreach ($options['attr'] as $index => $option) {
                $form->add('offer_' . $option['type'], CollectionType::class, [
                        'label' => $option['type'],
                        'mapped' => false,
                        'allow_add'    => true,
                        'attr' => [
                            'class' => 'dynamically-field'
                        ],
                        'entry_type'   => EntryType::class,
                        'entry_options'  => [
                            'fields' => [
                                [
                                    'name' => $option['type'],
                                    'type'    => TextType::class,
                                    'options' => []
                                ], [
                                    'name' => 'is_static',
                                    'type'    => CheckboxType::class,
                                    'options' => []
                                ]
                            ],
                            'attr' => [
                                'class' => 'collection-fields-wrapper'
                            ],
                        ],
                    ]
                );
            }
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                // this would be your entity, i.e. SportMeetup
                $data = $event->getData();

                $formModifier($event->getForm(), $data->getType());
            }
        );

        $builder->get('type')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier){
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $type = $event->getForm()->getData()->getId();

                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback functions!
                $formModifier($event->getForm()->getParent(), $type);
            }
        );

//        $builder->addEventListener(
//            FormEvents::SUBMIT,
//            function (FormEvent $event){
//                $f = $event->getForm();
//                $data = $event->getForm()->get('categories')->getData();
//                \Doctrine\Common\Util\Debug::dump($data);
//            }
//        );
    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
//            'data_class'      => 'YmlBundle\Entity\YmlType',
//            'csrf_protection' => false,
        ));
    }

}