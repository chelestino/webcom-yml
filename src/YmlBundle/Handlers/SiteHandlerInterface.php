<?php
/**
 * Created by PhpStorm.
 * User: G.Haritonov
 * Date: 03.11.2016
 * Time: 9:26
 */
namespace YmlBundle\Handlers;
use YmlBundle\Container\SettingsContainer;

/**
 * Handle method for custom info processing for each site
 *
 * Interface SiteHandlerInterface
 * @package YmlBundle\Handlers
 */
interface SiteHandlerInterface
{
    public function handle(SettingsContainer $info) : SettingsContainer;
}