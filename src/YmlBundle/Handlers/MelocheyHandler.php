<?php

/**
 * Created by PhpStorm.
 * User: G.Haritonov
 * Date: 03.11.2016
 * Time: 9:24
 */
namespace YmlBundle\Handlers;

use YmlBundle\Container\SettingsContainer;

class MelocheyHandler implements SiteHandlerInterface
{
    private function filterByPrice(array $var) : bool
    {
        if (isset($var['price'][0])) {
            if ($var['price'][0] != '') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function guessCurrency(int $price) : string
    {
        if (($price < 100000)) {
            return 'BYN';
        } else {
            return 'BYR';
        }
    }

    private function findByName(string $needle, array $haystack)
    {
        if (!empty($haystack)) {
            foreach ($haystack as $key => $value) {
                if ($value['name'] == $needle) {
                    return $key;
                }
            }
            return false;
        } else {
            return false;
        }
    }

    private function recTrim(array $value) : array
    {
        $result = [];
        foreach ($value as $val) {
            $result[] = preg_replace('/^[\pZ\pC]+|[\pZ\pC]+$/u', '', $val);
        }
        return $result;
    }

    public function handle(SettingsContainer $info) : SettingsContainer
    {
        $info['offers'] = array_filter($info['offers'], [$this, 'filterByPrice']);
        if (empty($info['offers'])) {
            throw new HandlerException('No offers found');
        }
        $categories = [];
        $i = 0;
        $cat_id = 1;
        foreach ($info['offers'] as $key => $offer) {
            ++$i;
            $offer['delivery'] = true;
            $offer['text'] = $offer['vendor'][0];
            $matches = [];
            if (preg_match('/id="product-(\d*?)"/uim', $offer['content_html'], $matches)) {
                $offer['id'] = $matches[1];
            } else {
                $offer['id'] = $i;
            }
            $offer['price'][0] = preg_replace('/\D/', '', $offer['price'][0]);
            if ($offer['price'][0] == '') {
                unset($info->offers[$key]);
                continue;
            }

            $matches = [];
            $offer['vendor'][0] = '';
            if (preg_match('/производитель:?\s*(.*?)</uim', $offer['content_html'], $matches)) {
                $offer['vendor'][0] = trim($matches[1]);
            }
            /*if ($offer['vendor'][0] == '' && preg_match('/производитель<\/td>.*?<td>(.*?)<\/td>/uims', $offer['content_html'], $matches)) {
                $offer['vendor'][0] = trim($matches[1]);
            }*/
            else/*if ($offer['vendor'][0] == '')*/ {
                $offer['vendor'][0] = 'Китай';
            }
            if ($offer['vendor'][0] == "") {
                $offer['vendor'][0] = 'Китай';
            }
            if (isset($offer['description'])) {
                unset($offer['description']);
            }
            /*$matches = [];
            if (preg_match('/id="tab-description"/miu', $offer['content_html']) && preg_match('/id="tab-description".*?<p>((?!<).{50,}?)<\/p>/uims', $offer['content_html'], $matches)) {
                $offer['description'][0] = trim(strip_tags($matches[1]));
            }*/
            if (isset($offer['country_of_origin'][0])) {
                $offer['country_of_origin'][0] = explode(':', $offer['country_of_origin'][0]);
                $offer['country_of_origin'][0] = trim($offer['country_of_origin'][0][1]);
            }
            if ($this->guessCurrency((int)$offer['price'][0]) == 'BYR') {
                $offer['price'][0] = (int)($offer['price'][0] / 10000);
            }
            $offer['currency_id'] = 'BYN';
            //$offer['currency_id'] = $this->guessCurrency((int)$offer['price'][0]);

            /*if (($cat_key = array_search($offer['typePrefix'][0], $categories)) !== false) {
                $offer['category_id'] = $cat_key + 1;
            } else {
                $categories[] = $offer['typePrefix'][0];
                end($categories);
                $cat_key = key($categories);
                $offer['category_id'] = $cat_key + 1;
            }*/

            $offer['crumbs'] = explode('/ ', $offer['crumbs']);
            $offer['crumbs'] = $this->recTrim($offer['crumbs']);
            if (count($offer['crumbs']) == '4' || count($offer['crumbs']) == '5') {
                if (($cat_key = $this->findByName($offer['crumbs'][1], $categories)) !== false) {
                    if (($child_cat_key = array_search($offer['crumbs'][2], $categories[$cat_key])) !== false) {
                        $offer['category_id'] = $child_cat_key;
                    } else {
                        $categories[$cat_key][$cat_id] = $offer['crumbs'][2];
                        $offer['category_id'] = $cat_id;
                        $cat_id++;
                    }
                } else {
                    $categories[$cat_id]['name'] = $offer['crumbs'][1];
                    $categories[$cat_id][$cat_id + 1] = $offer['crumbs'][2];
                    $offer['category_id'] = $cat_id;
                    $cat_id += 2;
                }
            } elseif (count($offer['crumbs']) == '3') {
                if (($cat_key = $this->findByName($offer['crumbs'][1], $categories)) !== false) {
                    $offer['category_id'] = $cat_key;
                } else {
                    $categories[$cat_id]['name'] = $offer['crumbs'][1];
                    $offer['category_id'] = $cat_id;
                    $cat_id++;
                }
            }
            $info->offers[$key] = $offer;
        }
        //$categories = array_combine(range(1, count($categories)), array_values($categories));
        //$categories = array_filter($categories);
        $info['categories'] = $categories;
        return $info;
    }
}