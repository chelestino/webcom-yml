<?php
/**
 * Created by PhpStorm.
 * User: G.Haritonov
 * Date: 14.11.2016
 * Time: 16:12
 */

namespace YmlBundle\Handlers;

use YmlBundle\Container\SettingsContainer;

class ShtukaturkiHandler implements SiteHandlerInterface
{
    private function filterByPrice(array $var) : bool
    {
        if (isset($var['price'][0])) {
            $var['price'][0] = explode('от', $var['price'][0]);
            $var['price'][0] = preg_replace('/[^0-9.]+/', '', $var['price'][0][1]);
            $var['price'][0] = substr($var['price'][0], 0, -1);
            if ($var['price'][0] != '') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function findByName(string $needle, array $haystack)
    {
        if (!empty($haystack)) {
            foreach ($haystack as $key => $value) {
                if ($value['name'] == $needle) {
                    return $key;
                }
            }
            return false;
        } else {
            return false;
        }
    }

    private function recTrim(array $value) : array
    {
        $result = [];
        foreach ($value as $val) {
            $result[] = preg_replace('/^[\pZ\pC]+|[\pZ\pC]+$/u', '', $val);
        }
        return $result;
    }

    public function handle(SettingsContainer $info) : SettingsContainer
    {
        $info['offers'] = array_filter($info['offers'], [$this, 'filterByPrice']);
        if (empty($info['offers'])) {
            throw new HandlerException('No offers found');
        }
        $categories = [];
        $i = 0;
        $cat_id = 1;
        foreach ($info['offers'] as $key => $offer) {
            ++$i;
            $offer['id'] = $i;
            $offer['delivery'] = false;
            $offer['pickup'] = true;
            $offer['price'][0] = explode('от', $offer['price'][0]);
            $offer['price'][0] = preg_replace('/[^0-9.]+/', '', $offer['price'][0][1]);
            $offer['price'][0] = substr($offer['price'][0], 0, -1);

            //$offer['country_of_origin'][0] = '';

            $offer['currency_id'] = "BYN";
            $offer['vendor'][0] = 'Giolli Industria colori srl';
            $offer['description'][0] = trim(preg_replace('/\s+/', ' ', $offer['description'][0]));

            $offer['crumbs'] = preg_split("/→/", $offer['crumbs']);
            $offer['crumbs'] = $this->recTrim($offer['crumbs']);
            if (count($offer['crumbs']) == '4') {
                if (($cat_key = $this->findByName($offer['crumbs'][2], $categories)) !== false) {
                    $offer['category_id'] = $cat_key;
                } else {
                    $categories[$cat_id]['name'] = $offer['crumbs'][2];
                    $offer['category_id'] = $cat_id;
                    $cat_id++;
                }
            } else {
                unset($info->offers[$key]);
                continue;
            }
            $info->offers[$key] = $offer;
        }

        $info['categories'] = $categories;
        return $info;
    }
}