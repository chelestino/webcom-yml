<?php
/**
 * Created by PhpStorm.
 * User: G.Haritonov
 * Date: 14.11.2016
 * Time: 16:12
 */

namespace YmlBundle\Handlers;

use YmlBundle\Container\SettingsContainer;

class BelgazHandler implements SiteHandlerInterface
{
    private function filterByPrice(array $var) : bool
    {
        if (isset($var['price'][0])) {
            $var['price'][0] = explode('/', $var['price'][0]);
            $var['price'][0] = preg_replace('/[^0-9]+/', '', $var['price'][0][0]);
            //$var['price'][0] = substr($var['price'][0], 0, -1);
            if ($var['price'][0] != '') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function findByName(string $needle, array $haystack)
    {
        if (!empty($haystack)) {
            foreach ($haystack as $key => $value) {
                if ($value['name'] == $needle) {
                    return $key;
                }
            }
            return false;
        } else {
            return false;
        }
    }

    private function recTrim(array $value) : array
    {
        $result = [];
        foreach ($value as $val) {
            $result[] = preg_replace('/^[\pZ\pC]+|[\pZ\pC]+$/u', '', $val);
        }
        return $result;
    }

    private function tdrows($elements)
    {
        $str = "";
        foreach ($elements as $element) {
            $str .= $element->nodeValue . "|";
        }

        return $str;
    }

    public function handle(SettingsContainer $info) : SettingsContainer
    {
        $info['offers'] = array_filter($info['offers'], [$this, 'filterByPrice']);
        if (empty($info['offers'])) {
            throw new HandlerException('No offers found');
        }
        $categories = [];
        $i = 0;
        $cat_id = 1;
        foreach ($info['offers'] as $key => $offer) {
            ++$i;
            $offer['id'] = $i;
            $offer['delivery'] = false;
            $offer['pickup'] = true;
            $offer['price'][0] = explode('/', $offer['price'][0]);
            $offer['price'][0] = preg_replace('/[^0-9.]+/', '', $offer['price'][0][0]);
            $offer['price'][0] = $offer['price'][0] . '.00';
            //$offer['country_of_origin'][0] = '';
            $offer['currency_id'] = "BYN";
            $offer['vendor'][0] = 'ГАЗ';

            //params
            $DOM = new \DOMDocument;
            $DOM->loadHTML('<?xml encoding="utf-8" ?>' . $offer['content_html']);

            $items = $DOM->getElementsByTagName('tr');
            $params = [];
            foreach ($items as $node) {
                $params[] = $this->tdrows($node->childNodes);
            }
            unset($offer['description']);
            $exclude = ['Главные', 'Модель:', 'Марка:', 'Цена', 'Основные', 'Дополнительные'];
            foreach ($params as $param_key => $param) {
                $param = explode('|', $param);
                $param = $this->recTrim($param);
                $offer['params'][$param_key]['name'] = $param[0];
                $offer['params'][$param_key]['value'] = $param[2];
                if (strpos($offer['params'][$param_key]['name'], ',') !== false) {
                    $unit = explode(', ', $offer['params'][$param_key]['name']);
                    $offer['params'][$param_key]['name'] = $unit[0];
                    $offer['params'][$param_key]['unit'] = $unit[1];
                }
                if ($offer['params'][$param_key]['name'] == 'Общее описание транспортного средства') {
                    $offer['description'][0] = $offer['params'][$param_key]['value'];
                    unset($offer['params'][$param_key]);
                } elseif (in_array($offer['params'][$param_key]['name'], $exclude)) {
                    unset($offer['params'][$param_key]);
                }
            }
            $offer['description'][0] = trim(preg_replace('/\s+/', ' ', $offer['description'][0]));

            $offer['crumbs'] = preg_split("/\//", $offer['crumbs']);
            $offer['crumbs'] = $this->recTrim($offer['crumbs']);
            if (count($offer['crumbs']) == '4') {
                if (($cat_key = $this->findByName($offer['crumbs'][2], $categories)) !== false) {
                    $offer['category_id'] = $cat_key;
                } else {
                    $categories[$cat_id]['name'] = $offer['crumbs'][2];
                    $offer['category_id'] = $cat_id;
                    $cat_id++;
                }
            } else {
                unset($info->offers[$key]);
                continue;
            }
            $info->offers[$key] = $offer;
        }

        $info['categories'] = $categories;
        return $info;
    }
}