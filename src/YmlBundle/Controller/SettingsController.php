<?php

namespace YmlBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use YmlBundle\Entity\Settings;
use YmlBundle\Form\SettingsType;
use YmlBundle\Entity\YmlType;
use YmlBundle\Entity\YmlFieldToType;

/**
 * Settings controller.
 *
 * @Route("/site/{siteId}/settings")
 */
class SettingsController extends Controller
{

    /**
     * Displays a form to edit an existing Settings entity.
     *
     * @Route("/", name="settings")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request)
    {
        $site_id = $request->attributes->get('siteId');
        $em = $this->getDoctrine()->getManager();
        $setting = $em->getRepository('YmlBundle:Settings')->findBySite($site_id);
        $setting = $setting[0];

        $editForm = $this->createForm('YmlBundle\Form\SettingsType', $setting);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->persist($setting);
            $em->flush();

            return $this->redirectToRoute('settings', array('siteId' => $site_id));
        }

        $ymlTypeFields = [];
        if($ymlType = $request->get('yml_type')) {
            $ymlFields = $em->getRepository('YmlBundle:YmlFieldToType')->findByType($ymlType['type']);

            foreach ($ymlFields as $index => $ymlField) {
                $ymlTypeFields['attr'][$index]['id'] = $ymlField->getField()->getId();
                $ymlTypeFields['attr'][$index]['type'] = $ymlField->getField()->getField();
            }
        }

        $ymlTypes = new YmlType();
        $ymlTypeForm = $this->createForm('YmlBundle\Form\YmlTypeType', $ymlTypes, $ymlTypeFields);
        $ymlTypeForm->handleRequest($request);

        $fieldDataJson = $setting->getValue();
        if ($ymlTypeForm->isSubmitted() && $ymlTypeForm->isValid()) {
            $ymlTypeFieldsData = $request->get('yml_type');

            $this->prepareFormData($ymlTypeFieldsData);
            $fieldDataJson = json_encode($ymlTypeFieldsData);
        }

        $crawledSiteForm = $this->createForm('YmlBundle\Form\CrawledSiteType');
        $crawledSiteForm->handleRequest($request);

        return $this->render('settings/edit.html.twig', array(
            'setting' => $setting,
            'fieldDataJson' => $fieldDataJson,
            'edit_form' => $editForm->createView(),
            'yml_type_form' => $ymlTypeForm->createView(),
            'crawled_site' => $crawledSiteForm->createView(),
        ));
    }

    public function prepareFormData(&$haystack)
    {
        foreach ($haystack as $key => $value) {
            if (is_array($value)) {
                $haystack[$key] = $this->prepareFormData($haystack[$key]);
            }

            if (empty($haystack[$key])) {
                unset($haystack[$key]);
            }
        }

        return $haystack;
    }


}
