<?php
/**
 * Created by PhpStorm.
 * User: G.Haritonov
 * Date: 09.11.2016
 * Time: 12:11
 */
namespace YmlBundle\Container;

class SettingsContainer implements \ArrayAccess
{
    public $name;

    public $company;

    public $url;

    public $type;

    public $base_url;

    public function __construct(array $settings = [])
    {
        foreach ($settings as $key => $setting) {
            $this->$key = $setting;
        }
    }

    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    public function offsetExists($offset)
    {
        return isset($this->$offset);
    }

    public function offsetUnset($offset)
    {
        unset($this->$offset);
    }

    public function offsetGet($offset)
    {
        return isset($this->$offset) ? $this->$offset : null;
    }

    /**
     * Generates full file path to xml file based of site url.
     *
     * @param string $xml_dir
     * @return string
     */
    public function createFileName(string $xml_dir) : string
    {
        $filename = $this->createAlias();
        $filename = $xml_dir . DIRECTORY_SEPARATOR . $filename . '.xml';
        return $filename;
    }

    /**
     * Generates alias based on site url.
     *
     * @return string
     */
    public function createAlias() : string
    {
        $filename = parse_url($this->url);
        $filename = $filename['host'];
        $filename = str_ireplace('www.', '', $filename);
        $filename = preg_replace('/\.\S{2,}?/i', '', $filename);
        $filename = preg_replace('/[0-9]+/', '', $filename);
        $filename = ucwords($filename);
        return $filename;
    }
}