<?php
/**
 * Created by PhpStorm.
 * User: G.Haritonov
 * Date: 05.10.2016
 * Time: 12:38
 */

namespace YmlBundle\Services;

class Yml
{
    protected $entityManager;

    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getAll()
    {
        $em = $this->entityManager;
        $ymlList = $em->getRepository('YmlBundle:YmlType')->findAll();
        $list = array();
        foreach ($ymlList as $yml) {
            $list[] = $yml->getType();
        }

        return $list;
    }

    public function getById($id)
    {
        $em = $this->entityManager;
        $yml = $em->getRepository('YmlBundle:YmlType')->findById($id);
        return $yml;
    }

}