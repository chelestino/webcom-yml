<?php
/**
 * Created by PhpStorm.
 * User: G.Haritonov
 * Date: 05.10.2016
 * Time: 15:38
 */

namespace YmlBundle\Services;

use blainerohmer\YmlGenerator\Generator;
use blainerohmer\YmlGenerator\Model\Category;
use blainerohmer\YmlGenerator\Model\Currency;
use blainerohmer\YmlGenerator\Model\Delivery;
use blainerohmer\YmlGenerator\Model\Offer\OfferCustom;
use blainerohmer\YmlGenerator\Model\Offer\OfferParam;
use blainerohmer\YmlGenerator\Model\ShopInfo;
use blainerohmer\YmlGenerator\Settings;
use YmlBundle\Container\SettingsContainer;

class YmlFormatter implements FormatterInterface
{
    private $xml_dir;

    public function __construct($xml_dir)
    {
        $this->xml_dir = $xml_dir;
    }

    /**
     * @param SettingsContainer $info
     * @return string
     */
    public function create(SettingsContainer $info)
    {
        $file = $info->createFileName($this->xml_dir);

        $settings = (new Settings())
            ->setOutputFile($file);

        // Creating ShopInfo object (https://yandex.ru/support/webmaster/goods-prices/technical-requirements.xml#shop)
        $shopInfo = (new ShopInfo())
            ->setName($info['name'])
            ->setCompany($info['company'])
            ->setUrl($info['url']);

        // Creating currencies array (https://yandex.ru/support/webmaster/goods-prices/technical-requirements.xml#currencies)
        $currencies = [];
        foreach ($info['currencies'] as $currency) {
            $currencies[] = (new Currency())
                ->setId($currency['currency_id'])
                ->setRate($currency['currency_rate']);
        }
        // Creating categories array (https://yandex.ru/support/webmaster/goods-prices/technical-requirements.xml#categories)
        $categories = [];
        foreach ($info['categories'] as $key => $category) {
            $categories[] = (new Category())
                ->setId($key)
                ->setName($category['name']);
            unset($category['name']);
            foreach ($category as $child_key => $child_cat) {
                $categories[] = (new Category())
                    ->setId($child_key)
                    ->setName($child_cat)
                    ->setParentId($key);
            }
        }
        // Creating delivery-options array
        $deliveries = [];
        if (!empty($info['delivery_options'])) {
            foreach ($info['delivery_options'] as $key => $delivery) {
                $deliveries[] = (new Delivery())
                    ->setCost($delivery['cost'])
                    ->setDays($delivery['days'] ?? '0')
                    ->setOrderBefore($delivery['order-before']);
            }
        }
        // Creating offers array (https://yandex.ru/support/webmaster/goods-prices/technical-requirements.xml#offers)
        $offers = [];
        //TODO: Encapsulate this.
        switch ($info['type']) {
            case '2':
                foreach ($info['offers'] as $key => $offer) {
                    $offer_object = (new OfferCustom())
                        ->setId($offer['id'])
                        ->setAvailable(true)
                        ->setUrl($offer['url'])
                        ->setPrice($offer['price'][0])
                        ->setCurrencyId($offer['currency_id'])
                        ->setCategoryId($offer['category_id'])
                        ->setVendor($offer['vendor'][0])
                        ->setDescription($offer['description'][0])
                        ->setCountryOfOrigin($offer['country_of_origin'][0])
                        ->setModel($offer['model'][0]);
                    if ($offer['delivery']) {
                        $offer_object->setDelivery(true);
                    }   
                    if ($offer['pickup']) {
                        $offer_object->setPickup(true);
                    }
                    if (isset($offer['params'])) {
                        foreach ($offer['params'] as $param) {
                            $offer_object->addParam((new OfferParam())->setName($param['name'])->setUnit($param['unit'])->setValue($param['value']));
                        }
                    }

                    $offers[] = $offer_object;
                }
                break;
        }

        (new Generator($settings))->generate(
            $shopInfo,
            $deliveries,
            $currencies,
            $categories,
            $offers
        );

        return $file;
    }
}