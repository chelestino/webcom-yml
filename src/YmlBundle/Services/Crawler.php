<?php
/**
 * Created by PhpStorm.
 * User: G.Haritonov
 * Date: 05.10.2016
 * Time: 12:38
 */

namespace YmlBundle\Services;

use Symfony\Component\DomCrawler\Crawler as DOMCrawler;
use YmlBundle\Container\SettingsContainer;

class Crawler extends \PHPCrawler
{
    private $rules = SettingsContainer::class;

    private $result = SettingsContainer::class;

    public function __construct(SettingsContainer $settingsContainer)
    {
        $this->result = $settingsContainer;
        parent::__construct();
    }

    public function setParseRules(SettingsContainer $rules)
    {
        $this->rules = $rules;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function getRules()
    {
        return $this->rules;
    }

    function handleDocumentInfo(\PHPCrawlerDocumentInfo $PageInfo)
    {
        $parser = new DOMCrawler($PageInfo->content);

        $offerHash = md5($PageInfo->url);

        foreach ($this->rules as $key => $rule) {
            $this->result[$key] = $rule;
            if (substr_count($key, 'offer_')) {
                unset($this->result[$key]);
                $typeDescriptionElement = str_replace('offer_', '', $key);
                foreach ($rule as $index => $item) {
                    if (empty($item['is_static'])) {
                        $k = (array_keys($item));

                        $this->result->offers[$offerHash][$typeDescriptionElement] = $parser->filterXPath($item[$k[0]])->each(function (DOMCrawler $node, $i) {
                            return $node->text();
                        });
                        if ($parser->filterXPath($this->rules['nav_chain'])->count()) {
                            $this->result->offers[$offerHash]['crumbs'] = $parser->filterXPath($this->rules['nav_chain'])->text();
                        }
                        if ($parser->filterXPath($this->rules['content_html'])->count()) {
                            $this->result->offers[$offerHash]['content_html'] = $parser->filterXPath($this->rules['content_html'])->html();
                        }
                    }
                }
            }
        }
        $this->result->offers[$offerHash]['url'] = $PageInfo->url;
    }

    public function setupMeta()
    {
        $this->setConnectionTimeout(20);
        $this->setStreamTimeout(20);
        $this->addContentTypeReceiveRule("#text/html#");
        $this->addURLFilterRule('#(\.jpg|\.jpeg|\.gif|\.png|get_file|\.ico|\.ppt|\.xlsx|\.pdf|\.js|component|\.css|\?)#i');
        //$this->addURLFollowRule('#^product#i');
        $this->obeyNoFollowTags(true);
        //$this->setRequestLimit(150, true);
        $this->enableCookieHandling(true);
        $this->enableAggressiveLinkSearch(false);
        //$this->excludeLinkSearchDocumentSections(\PHPCrawlerLinkSearchDocumentSections::ALL_SPECIAL_SECTIONS); NOT WORKING OTHER WAY!
        $this->setFollowRedirects(true);
        $this->setFollowRedirectsTillContent(true);
    }
}