<?php
/**
 * Created by PhpStorm.
 * User: G.Haritonov
 * Date: 05.10.2016
 * Time: 15:38
 */

namespace YmlBundle\Services;

use YmlBundle\Container\SettingsContainer;
use YmlBundle\Handlers\HandlerException;
use YmlBundle\Handlers\SiteHandlerInterface;

class InfoHandler
{
    private $xml_dir;

    public function __construct($xml_dir)
    {
        $this->xml_dir = $xml_dir;
    }

    /**
     * Runs custom handler for site.
     *
     * @param SettingsContainer $info
     * @return SettingsContainer
     */
    public function process(SettingsContainer $info) : SettingsContainer
    {
        $file = $info->createAlias();
        $classname = 'YmlBundle\\Handlers\\' . $file . 'Handler';
        if (class_exists($classname) && method_exists($classname, 'handle')) {
            $handler = new $classname();
            if ($handler instanceof SiteHandlerInterface) {
                try {
                    $info = $handler->handle($info);
                } catch(HandlerException $e) {
                    //TODO: Show error to user proper way. (http://symfony.com/doc/current/event_dispatcher.html)
                    die($e->getMessage());
                }
            }
        }
        return $info;
    }
}
