<?php
/**
 * Created by PhpStorm.
 * User: G.Haritonov
 * Date: 16.11.2016
 * Time: 17:10
 */
namespace YmlBundle\Services;

use YmlBundle\Container\SettingsContainer;

interface FormatterInterface
{
    public function __construct($xml_dir);

    public function create(SettingsContainer $info);
}