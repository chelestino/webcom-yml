<?php
/**
 * Created by PhpStorm.
 * User: G.Haritonov
 * Date: 05.10.2016
 * Time: 15:38
 */

namespace YmlBundle\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use YmlBundle\Container\SettingsContainer;

class YmlDeployer
{
    private $xml_dir;
    
    private $api_key;

    public function __construct($xml_dir)
    {
        $this->xml_dir = $xml_dir;
    }

    /**
     * Simple POST request with file
     *
     * @param SettingsContainer $settings
     * @return Response
     */
    public function deploy(SettingsContainer $settings) : Response
    {
        $file = $settings->createFileName($this->xml_dir);
        $this->api_key = md5($settings->createAlias());
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $settings['url'],
            // You can set any number of default request options.
            'timeout'  => 2.0,
        ]);
        $response = $client->post($settings['url'] . '/yml_deploy.php?apikey=' . $this->api_key, [
            'multipart' => [
                [
                    'name'     => 'yml',
                    'contents' => fopen($file, 'r'),
                ],
            ],
        ]);
        return $response;
    }
}