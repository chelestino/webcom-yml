$(document).ready(function() {

    $('#yml_type_type').material_select();
    var $type = $('#yml_type_type');
// When sport gets selected ...
    $type.change(function() {
        // ... retrieve the corresponding form.
        var $form = $(this).closest('form');
        // Simulate form data, but only include the selected sport value.
        var data = {};
        data[$type.attr('name')] = $type.val();
        // Submit data via AJAX to the form's action path.
        $.ajax({
            url : $form.attr('action'),
            type: $form.attr('method'),
            data : data,
            success: function(html) {
                // Replace current position field ...
                $('#yml_type_field').replaceWith(
                    // ... with the returned one from the AJAX response.
                    $(html).find('.dynamically-field').parent('div')
                );
                $('#dinamically-fields-wrapper').wrapInner('<div id="yml_type_field"></div>');
                $('div.dynamically-field').each(function(i, el){

                    //var $newLinkLi = $('<li></li>');
                    //
                    //$(el).append($newLinkLi);
                    $(el).data('index', $(el).find(':input').length);

                    var prototype = $(el).data('prototype');

                    // get the new index
                    var index = $(el).data('index');

                    var newForm = prototype.replace(/__name__label__/g, '');
                    // Replace '$$name$$' in the prototype's HTML to
                    // instead be a number based on how many items we have
                    var newForm = newForm.replace(/__name__/g, index);

                    // increase the index with one for the next item
                    $(el).data('index', index + 1);

                    // Display the form in the page in an li, before the "Add a tag" link li
                    var $newFormLi = $('<li></li>').append(newForm);

                    $(el).append($newFormLi);

                });
            }
        });
    });

    var $form = $('#crawled_site');
    $form.submit(function(e){
        e.preventDefault();
        var $site = $('#crawled_site_url');
        var data = {};
        data[$site.attr('name')] = $site.val();

        $.ajax({
            url : $form.attr('action'),
            type: $form.attr('method'),
            data : data,
            success: function(data) {
                //console.log($(data).find('.crawled-site'));
                // Replace current position field ...
                $('.crawled-site').append(
                    // ... with the returned one from the AJAX response.
                    $(data).closest('.crawled-site')
                    //data
                );
            }
        });
    });

    var $settingsForm = $('#yml_type');
    $('#setting-post').click(function(){
        $settingsForm.submit();
    });

    $settingsForm.submit(function(e){
        e.preventDefault();

        var formData = $settingsForm.serialize();

        $.ajax({
            url : $form.attr('action'),
            type: $form.attr('method'),
            data : formData,
            success: function(data) {
                var fieldsDataJson = $(data).find('#fields-data-json').text();
                $('#settings_value').val(fieldsDataJson);
                $('#settings').submit();
            }
        });
    });

        // Get the ul that holds the collection of tags
        var $collectionHolder = $('ul.currencies, ul.categories, ul.delivery-options, ul.optional_site_fields, ul.optional_type_fields');

    // add the "add a tag" anchor and li to the tags ul
        $collectionHolder.each(function (i, el) {
            var $addTagLink = $('<a href="#" class="add_tag_link">+</a>');
            var $newLinkLi = $('<li></li>').append($addTagLink);

            $(el).append($newLinkLi);
            $(el).data('index', $collectionHolder.find(':input').length);

            $addTagLink.on('click', function(e) {
                // prevent the link from creating a "#" on the URL
                e.preventDefault();

                addTagForm($(el), $newLinkLi);
            });
        });

    //$('#yml_type_type').change(function(event) {
    //

    //    //if (event.target.tagName.toLowerCase() === 'li') {
    //    //    // do your action on your 'li' or whatever it is you're listening for
    //    //}
    //});

    function addTagForm($collectionHolder, $newLinkLi) {
        // Get the data-prototype explained earlier
        var prototype = $collectionHolder.data('prototype');

        // get the new index
        var index = $collectionHolder.data('index');

        // Replace '$$name$$' in the prototype's HTML to
        // instead be a number based on how many items we have
        var newForm = prototype.replace(/__name__/g, index);

        // increase the index with one for the next item
        $collectionHolder.data('index', index + 1);

        // Display the form in the page in an li, before the "Add a tag" link li
        var $newFormLi = $('<li></li>').append(newForm);

        // also add a remove button, just for this example
        $newFormLi.append('<a href="#" class="remove-tag">x</a>');

        $newLinkLi.before($newFormLi);

        // handle the removal, just for this example
        $('.remove-tag').click(function(e) {
            e.preventDefault();

            $(this).parent().remove();

            return false;
        });
    }

});